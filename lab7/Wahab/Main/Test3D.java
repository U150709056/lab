package Wahab.Main;
import Wahab.Shapes3D.Box;
import Wahab.Shapes3D.Cylinder;

public class Test3D
{
	public static void main(String[] main)
	{
		Cylinder cylinder = new Cylinder(5,6);
		
		System.out.println(cylinder.area());
		System.out.println(cylinder.volume());
		System.out.println(cylinder.toString());
		
		Box box = new Box(4,5,6);
		
		System.out.println(box.area());
		System.out.println(box.volume());
		System.out.println(box.toString());

	}
}
