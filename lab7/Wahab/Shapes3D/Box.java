package Wahab.Shapes3D;
import  Wahab.Shapes3D.Rectangle;

public class Box extends Rectangle
{
	double height;
	
	public Box(double width, double length, double height)
	{
		super(width,length);
		this.height = height;
	}
	public double area()
	{
		return 2 * super.area() + 2 * (width * height) + 2 * (length * height);
	}
	
	public double volume()
	{
		return super.area() * height;
	}
	
	public String toString()
	{
	 	return "Height" + height + "Length" + length + "Width" + width;
	}
}
	
