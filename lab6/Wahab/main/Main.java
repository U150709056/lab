package Wahab.main;
import Wahab.shapes.*;
import java.util.ArrayList;

public class Main

{
	public static void main(String[] main)
	{
		Circle circle1 = new Circle(5);
		Circle circle2 = new Circle(6);
		Circle circle3 = new Circle(7);
		
		System.out.println();
		System.out.println(circle1.area());
		System.out.println(circle2.area());
		System.out.println(circle3.area());
		System.out.println();	
		
		Square square = new Square(4);
		System.out.println(square.area());
		System.out.println();
		
		ArrayList<Circle> circ = new ArrayList<Circle>();
		circ.add(new Circle(5));
		circ.add(new Circle(6));
		circ.add(new Circle(7));
		
		for (int r=0; r<circ.size(); r++)
		{
			System.out.println(circ.get(r).area());
		} 
		System.out.println();
	}
}
