package Wahab.shapes;

public class Circle
{
	double radius;
	
	public Circle(int r)
	{
		radius = r;
	}
	
	public double area()
	{
		double res = 3.14 * Math.pow(radius,2);
		return res;
	}
}
