public class TestDoc
{
	public static void main(String[] test)
	{
		Rectangle rectangle = new Rectangle();
		rectangle.sideA = 5;
		rectangle.sideB = 6;
		int rectangleArea = rectangle.area();
		int rectanglePar = rectangle.parameter();

		System.out.println(rectangleArea);
		System.out.println(rectanglePar);
		
		
		Circle circle = new Circle();
		circle.radius = 10;
		double circleArea = circle.area();
		double circlePar = circle.parameter();

		System.out.println(circleArea);
		System.out.println(circlePar);
	}
}
