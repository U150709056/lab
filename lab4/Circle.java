
public class Circle
{
	int radius;

	public double area()
	{
		double cirArea = 3.14 * (Math.pow(radius, 2));
		return cirArea;
	}
	public double parameter()
	{
		double cirpar = 2 * 3.14 * radius;
		return cirpar;
	}

}
